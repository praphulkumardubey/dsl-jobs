package builders
import groovy.transform.builder.Builder
import groovy.transform.builder.SimpleStrategy
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job

@Builder(builderStrategy = SimpleStrategy, prefix = '')
class AppBuild {
  String jobName
  String description = 'Build and test the app.'
  String projectName
  String labelTag
  String randomGenerator
  String endPointURL
      
  Job build(DslFactory dslFactory) {
    dslFactory.job(this.jobName) {
      label(this.labelTag)
      it.description this.description
      logRotator {
        numToKeep 5
      }
      notifications {
        endpoint(this.endPointURL+"-"+this.projectName+"-jobstatus", 'HTTP', 'JSON') {
            event('all')
            timeout(30000)
            logLines(0)
        }
    }
      parameters {
        stringParam('jobref', '000000', 'job ref default value is 000000')
    }
      authenticationToken("app-build-"+this.projectName+"-"+this.randomGenerator)
      
      steps {
        shell theBuildScript()

      }        
     
    }
  }     

  String theBuildScript() {
    return """
      #!/bin/bash
      sleep 3
    """.stripIndent().trim()
  }     

}