job("create-app-jobs") {
	description()
	keepDependencies(false)
	scm {
        git('https://praphulkumardubey@bitbucket.org/praphulkumardubey/dsl-jobs.git', 'master')
    }
	parameters {
        stringParam('app', 'cda', 'job ref default value is cda')
    }

  	steps {
		dsl(['cdaCI.groovy'])
	}	
}