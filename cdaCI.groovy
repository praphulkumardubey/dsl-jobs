import builders.AppBuild
import org.apache.commons.lang.RandomStringUtils
import jenkins.model.Jenkins
import hudson.model.User
import hudson.security.Permission
import hudson.EnvVars

String app
String endpointurl

def generator(createfile) {

    String charset = (('A'..'Z') + ('a'..'z')).join()
    String fileName= WORKSPACE + "/" + createfile
    File inputFile = new File(fileName)
    inputFile.createNewFile()
    Integer length = 20
    String randomString = RandomStringUtils.random(length, charset.toCharArray())
    inputFile.write(randomString)
    return randomString
}
 
folder(this.app) {
    displayName(this.app)
    description('Creating Folder for jobs')
}

//app-build
new AppBuild()
    .jobName(this.app+'/app-build-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/app-build')
    .randomGenerator(generator("app-build-"+this.app+".txt"))
    .build(this)
    
//app-codequality
new AppBuild()
    .jobName(this.app+'/app-codequality-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/app-codequality')
    .randomGenerator(generator("app-codequality-"+this.app+".txt"))
    .build(this)

//app-config
new AppBuild()
    .jobName(this.app+'/app-config-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/app-config')
    .randomGenerator(generator("app-config-"+this.app+".txt"))
    .build(this)

//app-deploy
new AppBuild()
    .jobName(this.app+'/app-deploy-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/app-deploy')
    .randomGenerator(generator("app-deploy-"+this.app+".txt"))
    .build(this)

//app-prereq
new AppBuild()
    .jobName(this.app+'/app-prereq-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/app-prereq')
    .randomGenerator(generator("app-prereq-"+this.app+".txt"))
    .build(this)

//app-release
new AppBuild()
    .jobName(this.app+'/app-release-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/app-release')
    .randomGenerator(generator("app-release-"+this.app+".txt"))
    .build(this)
    
//deploy-maven
new AppBuild()
    .jobName(this.app+'/deploy-maven-build-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/deploy-maven-build')
    .randomGenerator(generator("deploy-maven-build-"+this.app+".txt"))
    .build(this)

//infra-config
new AppBuild()
    .jobName(this.app+'/infra-config-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/infra-config')
    .randomGenerator(generator("infra-config-"+this.app+".txt"))
    .build(this)

//infra-decomm
new AppBuild()
    .jobName(this.app+'/infra-decomm-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/infra-decomm')
    .randomGenerator(generator("infra-decomm-"+this.app+".txt"))
    .build(this)

//infra-mon
new AppBuild()
    .jobName(this.app+'/infra-mon-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/infra-mon')
    .randomGenerator(generator("infra-mon-"+this.app+".txt"))
    .build(this)

//infra-prov
new AppBuild()
    .jobName(this.app+'/infra-prov-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/infra-prov')
    .randomGenerator(generator("infra-prov-"+this.app+".txt"))
    .build(this)

//infra-secure
new AppBuild()
    .jobName(this.app+'/infra-secure-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/infra-secure')
    .randomGenerator(generator("infra-secure-"+this.app+".txt"))
    .build(this)

//invoke-maven-build
new AppBuild()
    .jobName(this.app+'/invoke-maven-build-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/invoke-maven-build')
    .randomGenerator(generator("invoke-maven-build-"+this.app+".txt"))
    .build(this)

//invoke-msbuild-build
new AppBuild()
    .jobName(this.app+'/invoke-msbuild-build-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/invoke-msbuild-build')
    .randomGenerator(generator("invoke-msbuild-build-"+this.app+".txt"))
    .build(this)

//testing-api
new AppBuild()
    .jobName(this.app+'/testing-api-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/testing-api')
    .randomGenerator(generator("testing-api-"+this.app+".txt"))
    .build(this)

//testing-fun
new AppBuild()
    .jobName(this.app+'/testing-fun-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/testing-fun')
    .randomGenerator(generator("testing-fun-"+this.app+".txt"))
    .build(this)

//testing-mob
new AppBuild()
    .jobName(this.app+'/testing-mob-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/testing-mob')
    .randomGenerator(generator("testing-mob-"+this.app+".txt"))
    .build(this)

//testing-pnv
new AppBuild()
    .jobName(this.app+'/testing-pnv-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/testing-pnv')
    .randomGenerator(generator("testing-pnv-"+this.app+".txt"))
    .build(this)

//testing-sec
new AppBuild()
    .jobName(this.app+'/testing-sec-'+this.app)
    .projectName(this.app)
    .labelTag('master')
    .endPointURL(this.endpointurl+'/testing-sec')
    .randomGenerator(generator("testing-sec-"+this.app+".txt"))
    .build(this)
